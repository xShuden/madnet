		--target : target url or ip
		--thread : thread. Default: 800
		--multi : multiplication. Default: 50
		--file : another proxy file. Default : proxy.txt
		--update : update proxy
		--check : check proxy

	  Usage:
		--target <target>
		--target <target> --file <proxy_file>
		--target <target> --multi <multi>
		--target <target> --thread <thread>
		--target <target> --multi <multi> --thread <thread>
		--target <target> --file <proxy_file> --multi <multi>
		--target <target> --file <proxy_file> --thread <thread>
		--target <target> --file <proxy_file> --multi <multi> --thread <thread>
		--check --file <proxy_file>
		--update
		--check

  Kullanımı yukarıdaki gibidir.
  Verilmeyen her argümanın değeri yerine  default değer kullanılacaktır.

--multi : Aynı proxy ile ard arda kaç kere istek yapılacağıdır.
--thread : kaç thread açılsın. 
--file : proxylerin indireleceği dosya.
--update : proxyleri kaynaklardan çekmemize yarar.
--check : çekilen proxyler kontrol edilir. Kötü proxyler silinir.
--target : Hedef, ister ip olarak ister url istersek de domain olarak verebiliriz. 



 
